using UnityEngine;
using System.Collections;

public class BalaScript : MonoBehaviour {
	
	
	public float BalaSpeed= 10.0F;
	public float TimeToLive = 5;
	private float CurrentTimeToLive = 0;
	
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	    transform.Translate(Vector3.forward*BalaSpeed);
		CurrentTimeToLive += Time.deltaTime;
		if(CurrentTimeToLive > TimeToLive)
		Destroy(gameObject);
		 
		
	}
}