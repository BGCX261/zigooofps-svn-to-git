3D model and texture maps created by Marv Mays for Mesh Factory.

Distribution of this archive and/or its contents without
this readme file is prohibited.

Model Copyright 1998-2006 Mesh Factory. All Rights Reserved.

---------------------------------------------------------

For more 3D models visit us at http://www.meshfactory.com

---------------------------------------------------------

Questions about our 3D model products? Contact Us:

Mesh Factory
1605 Terrawenda Drive
Defiance, OH 43512
USA

email: sales@meshfactory.com
fax: (561)431-2648

